﻿using System;

namespace Oefening_26._2
{
    internal class Tv : ElektrischToestel
    {
        private int _beeldgrootte;
        private int _herz;
        private int _kanaal = 0;
        private int _volume = 0;
        private bool _teletekst;

        public Tv() : base() { }
        public Tv(string merk, string type, int herz, int beeldgrootte, string afbeelding) : base(merk, type, afbeelding)
        {
            Beeldgrootte = beeldgrootte;
            Herz = herz;

        }

        public int Beeldgrootte
        {
            get { return _beeldgrootte; }
            set { _beeldgrootte = value; }
        }
        public int Herz
        {
            get { return _herz; }
            set
            {

                if (value >= 0 && int.TryParse(value.ToString(), out int resultaat))
                {
                    _herz = value;
                }
                else
                {
                    throw new Exception("Herz kan niet kleiner zijn dan 0!");
                }
            }
        }
        public int Kanaal
        {
            get { return _kanaal; }
            set
            {

                if (value >= 0 && int.TryParse(value.ToString(), out int resultaat))
                {
                    _kanaal = value;
                }
                else
                {
                    throw new Exception("Het kanaal kan niet kleiner zijn dan 0!");
                }
            }
        }
        public int Volume
        {
            get { return _volume; }
            set
            {
                if (value >= 0 && int.TryParse(value.ToString(), out int resultaat))
                {
                    _volume = value;
                }
                else
                {
                    throw new Exception("Het volume kan niet kleiner zijn dan 0!");
                }

            }
        }
        public bool Tletekst
        {
            get { return _teletekst; }
            set { _teletekst = value; }
        }
        public override string ToString()
        {
            return base.ToString();
        }
    }
}
