﻿using System;
using System.Diagnostics;
using System.IO;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media.Imaging;
using System.Collections;
using System.Collections.Generic;

namespace Oefening_26._2
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow()
        {
            InitializeComponent();
        }

        List<Warmwaterkoker> warmwaterkokerList = new List<Warmwaterkoker>();

        Tv Sony = new Tv("Sony", "Bravia XR-50X94S", 50, 150, "/sony.jpeg");
        Tv Samsung = new Tv("Samsung", "QE50Q64CAUXXN", 100, 126, "/samsung.jpeg");
        Warmwaterkoker Uccello = new Warmwaterkoker("Ucello Warmwaterkoker", "Kantelbare Warmwaterkoker", "/Ucello.jpeg", 1500);
        Warmwaterkoker Hades = new Warmwaterkoker("Hades Cotswold", "Retro waterkoker Mint Groen", "/Hades.jpeg", 1700);

        private void rdoSony_Checked(object sender, RoutedEventArgs e)
        {
            imgUcello.Visibility = Visibility.Hidden;
            imgHades.Visibility = Visibility.Hidden;
            imgSamsung.Visibility = Visibility.Hidden;
            imgSony.Visibility = Visibility.Visible;
            //imgWeergave.Source = new BitmapImage(new Uri(Sony.Afbeelding, UriKind.RelativeOrAbsolute));
        }

        private void rdoSamsung_Checked(object sender, RoutedEventArgs e)
        {
            imgUcello.Visibility = Visibility.Hidden;
            imgHades.Visibility = Visibility.Hidden;
            imgSony.Visibility = Visibility.Hidden;
            imgSamsung.Visibility = Visibility.Visible;
            //imgWeergave.Source = new BitmapImage(new Uri(Samsung.Afbeelding, UriKind.RelativeOrAbsolute));
        }

        private void cmbWaterkokers_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            if (cmbWaterkokers.SelectedIndex == 0)
            {
                imgUcello.Visibility = Visibility.Visible;
                imgHades.Visibility = Visibility.Hidden;
                imgSony.Visibility = Visibility.Hidden;
                imgSamsung.Visibility = Visibility.Hidden;
            }
            if (cmbWaterkokers.SelectedIndex == 1)
            {
                imgHades.Visibility = Visibility.Visible;
                imgUcello.Visibility = Visibility.Hidden;
                imgSony.Visibility = Visibility.Hidden;
                imgSamsung.Visibility = Visibility.Hidden;
            }
        }

        private void Window_Loaded(object sender, RoutedEventArgs e)
        {
            warmwaterkokerList.Add(Uccello);
            warmwaterkokerList.Add(Hades);
            cmbWaterkokers.ItemsSource = warmwaterkokerList;
        }
    }
}
