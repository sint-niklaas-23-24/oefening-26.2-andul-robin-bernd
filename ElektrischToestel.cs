﻿namespace Oefening_26._2
{
    internal class ElektrischToestel
    {
        //atribuut
        private string _afbeelding;
        private string _merk;
        private bool _power = false;
        private string _type;
        //constructor
        public ElektrischToestel() { }
        public ElektrischToestel(string merk, string type, string afbeelding)
        {
            Merk = merk;
            Type = type;
            Afbeelding = afbeelding;
        }
        //properties
        public string Afbeelding
        {
            get { return _afbeelding; }
            set { _afbeelding = value; }
        }
        public string Merk
        {
            get { return _merk; }
            set { _merk = value; }
        }
        public string Type
        {
            get { return _type; }
            set { _type = value; }
        }
        public bool Power
        {
            get { return _power; }
            set { _power = value; }
        }
        public override string ToString()
        {
            return Merk + Type;
        }
    }
}
